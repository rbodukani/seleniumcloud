package com.google.test;

import java.lang.reflect.Method;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import io.github.bonigarcia.wdm.WebDriverManager;

public class GooglePageTest {

	@Test
	@Parameters("browsername")
	public void testGoogleTitle(String browsername) throws Exception {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.google.com");
		//driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
		Thread.sleep(5000);
		String title=driver.getTitle();
		Assert.assertEquals(title, "Google");
		driver.quit();
			
	}
	
	@Test		
	public void TestToFail()				
	{		
	    System.out.println("This method to test fail");					
	    SoftAssert softAssert = new SoftAssert();
	    softAssert.assertTrue(false);			
	}
	
	@Test
	public void extentPassTest(Method testMethod,XmlTest xmlTest) {
		String testName = xmlTest.getName();
		String method = testMethod.getName();
		ExtentReports extent = new ExtentReports();
		ExtentSparkReporter avent = new ExtentSparkReporter("/testreports/");
		extent.attachReporter(avent);
		ExtentTest test = extent.createTest(method);
		test.pass("Test Results are as expected");
	}
	
	
}
