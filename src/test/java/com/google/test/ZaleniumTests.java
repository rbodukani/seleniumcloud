package com.google.test;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.Browser;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.testng.internal.Version;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ZaleniumTests {

	RemoteWebDriver driver;
	DesiredCapabilities capabilities;
	
	@BeforeTest
	public void setupBeforeTest() throws MalformedURLException {
		
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		capabilities = new DesiredCapabilities(options);
//		ChromeOptions options = new ChromeOptions();
		capabilities = new DesiredCapabilities();
		//capabilities = DesiredCapabilities().
		//capabilities.setCapability("browserName", "Chrome");
		//capabilities.setCapability("version","latest");
		capabilities.setCapability(CapabilityType.PLATFORM_NAME,Platform.LINUX);
		//capabilities.setCapability(CapabilityType.BROWSER_NAME,Browser.CHROME);
		capabilities.setCapability("build",111.11);
		capabilities.setCapability("idleTimeout",180);
		capabilities.setCapability("recordVideo",true);
		capabilities.setCapability("tz","Asia/Kolkata");
		
		System.out.println(capabilities);
		URL url = new URL("http://3.128.76.190:4444/wd/hub");
		driver = new RemoteWebDriver(url,capabilities);
		
	}
	
	@Test
	public void testGoogleTitle() throws Exception {
		
//		WebDriverManager.chromedriver().setup();
//		
//		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.google.com");
		//driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
		Thread.sleep(5000);
		
//		driver.get("https://www.google.com");
//		driver.findElement(By.name("q")).sendKeys("Testing");
		String title=driver.getTitle();
		Assert.assertEquals(title, "Google");
		driver.quit();
			
	}
	
	@Test		
	public void TestToFail()				
	{		
	    System.out.println("This method to test fail");					
	    SoftAssert softAssert = new SoftAssert();
	    softAssert.assertTrue(true);
	}
	
	
}
