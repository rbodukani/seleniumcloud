package com.google.test;

import java.lang.reflect.Method;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.Browser;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.xml.XmlTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import io.github.bonigarcia.wdm.WebDriverManager;
import utils.ConfigFileReader;

public class BaseTestBeforeMethod{

	// private static Map<String,Map<TaskStatus,String>> testMethodName = new
	// HashMap<>();
	WebDriver driver;
	ExtentSparkReporter sparkReport;
	ExtentReports extentReport;
	ExtentTest extentTest;
	String gridUrl = ConfigFileReader.getPropertyValue("grid_url");
	
	@BeforeTest(alwaysRun=true)
	public void setBeforeTest(XmlTest xmlTest) {
		
		String testName = xmlTest.getName();

		// Step-1: create WebDriver instance
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss_SSS");
		String date = simpleDateFormat.format(new Date());

		sparkReport = new ExtentSparkReporter("./testreports/" + testName + date + ".html");
		extentReport = new ExtentReports();
		extentReport.attachReporter(sparkReport);
		
	}
	
	@BeforeMethod(alwaysRun=true)
	@Parameters({ "browsername", "runremote" })
	public void setupBeforeMethod(Method testMethod, XmlTest xmlTest, String browsername, String runremote) {

		if (runremote.contentEquals("yes")) {

			if (browsername.equals("chrome")) {

				//WebDriverManager.chromedriver().setup();
				//ChromeOptions options = new ChromeOptions();
				DesiredCapabilities capabilities = new DesiredCapabilities();
				//capabilities.setCapability(CapabilityType.PLATFORM_NAME, Platform.LINUX);
				//capabilities.setCapability(CapabilityType.BROWSER_NAME, Browser.CHROME);
				capabilities.setCapability("browserName", "chrome");
				capabilities.setCapability("browserVersion", "98.0");
				capabilities.setCapability("platformName", "Linux");
				capabilities.setCapability("idleTimeout", 180);
				capabilities.setCapability("recordVideo", true);
				capabilities.setCapability("tz", "Asia/Kolkata");
				URL url = null;
				try {
					url = new URL(gridUrl);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				driver = new RemoteWebDriver(url, capabilities);
			} else if (browsername.equals("firefox")) {

				//WebDriverManager.firefoxdriver().setup();
				//FirefoxOptions options = new FirefoxOptions();
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability(CapabilityType.PLATFORM_NAME, Platform.LINUX);
				capabilities.setCapability(CapabilityType.BROWSER_NAME, Browser.FIREFOX);
				capabilities.setCapability("build", 111.11);
				capabilities.setCapability("idleTimeout", 180);
				capabilities.setCapability("recordVideo", true);
				capabilities.setCapability("tz", "Asia/Kolkata");
				URL url = null;
				try {
					url = new URL(gridUrl);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				driver = new RemoteWebDriver(url, capabilities);
			}

		} else {
			if (browsername.equals("chrome")) {
				ChromeOptions chromeOptions = new ChromeOptions();
				WebDriverManager.chromedriver().setup();
				// WebDriverManager.chromedriver().driverVersion("85.0.4183.38").setup();
				driver = new ChromeDriver(chromeOptions);
			} else if (browsername.equals("firefox")) {
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
			}
		}

		// XmlSuite suite = testListener.getSuite().getXmlSuite();
		// Map<String,String> params = suite.getParameters();
		// XmlTest test = testListener.getCurrentXmlTest();
		// Map<String,String> caps = test.getLocalParameters();

		String method = testMethod.getName();
		extentTest = extentReport.createTest(method);
		extentTest.info("Test Execution started for method : " + method);
	}
	
	@AfterMethod(alwaysRun=true)
	public void teardownMethod() {
		driver.quit();
	}
	
	@AfterTest(alwaysRun=true)
	public void teardownTest() {
		extentReport.flush();
	}
}
