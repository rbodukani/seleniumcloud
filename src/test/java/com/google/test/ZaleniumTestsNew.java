package com.google.test;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ZaleniumTestsNew extends BaseTestBeforeMethod{

	@Test(groups= {"smoke","regression"})
	public void googleTest1() throws Exception {
		System.out.println("Test1 is getting started");
		System.out.println("Driver instance : "+ driver);
		driver.get("https://www.google.com");
		//driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
		Thread.sleep(5000);
//		driver.get("https://www.google.com");
//		driver.findElement(By.name("q")).sendKeys("Testing");
		String title=driver.getTitle();
		System.out.println("Title is "+title);
		extentTest.pass("Passed");
			
	}
	
	@Test(groups= {"smoke"})
	public void googleTest2() throws Exception {		
		System.out.println("Test2 is getting started");	
		driver.get("https://www.google.com");
		Thread.sleep(5000);
//		driver.get("https://www.google.com");
//		driver.findElement(By.name("q")).sendKeys("Testing");
		String title=driver.getTitle();
		System.out.println("Title is "+title);
		extentTest.fail("Failed");
	}
	
	
}
