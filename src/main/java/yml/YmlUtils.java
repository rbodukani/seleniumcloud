package yml;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;

import org.yaml.snakeyaml.Yaml;

public class YmlUtils {
	
	@SuppressWarnings("unchecked")
	public HashMap<String, String> getEmailProperties() throws Exception {
		
		String filepath = "/configuration/environments.yml";

		Yaml yaml = new Yaml();

		try (InputStream in = YmlUtils.class.getResourceAsStream(filepath)) {
			Iterable<Object> itr = yaml.loadAll(in);
			for (Object obj : itr) {

				for (Map.Entry<String, HashMap<String, HashMap<String, String>>> entry : ((HashMap<String, HashMap<String, HashMap<String, String>>>) obj).entrySet()) {

					if (entry.getKey().equals("email")) {

						HashMap<String, HashMap<String, String>> hm2 = entry.getValue();
						
						if (hm2.containsKey("QA")) {
							HashMap<String, String> hm3 = hm2.get("QA");
							in.close();
							return hm3;	

						}
					}
				}

			}
		}
		return null;
	}
	
	public String getFromAddress() throws Exception{
		HashMap<String,String> map =getEmailProperties();
		return map.get("from");
	}
	
	public String getToAddress() throws Exception{
		HashMap<String,String> map =getEmailProperties();
		return map.get("to");
	}

	public String getCcAddress() throws Exception{
		HashMap<String,String> map =getEmailProperties();
		return map.get("to");
		
	}

	
}

