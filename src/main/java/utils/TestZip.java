package utils;

import java.io.File;

public class TestZip {

	public static void main(String[] args) throws Exception {

		ZipUtility util = new ZipUtility();
		String attachment = "testreports";
		File dir = new File(attachment);
		String zipDirName = attachment+".zip";
		util.zipDirectory(dir, zipDirName);
	}

}
