package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {

	public static String getPropertyValue(String property) {
		
		String filepath = "src/main/resources/configuration/config.properties";
		File file = new File(filepath);
		try (FileInputStream inputStream = new FileInputStream(file)) {

			Properties prop = new Properties();
			prop.load(inputStream);
			return prop.getProperty(property);

		} catch (IOException io) {
			io.printStackTrace();
		}
		return filepath;

	}
}