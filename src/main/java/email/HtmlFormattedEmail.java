package email;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import jsoup.HtmlUtils;
import utils.FileUpload;
import utils.ZipUtility;
import yml.YmlUtils;


public class HtmlFormattedEmail {

	public static void sendEmailToQaGroup() throws Exception {
    	//Get Statuses
    	HashMap<String,String> map = HtmlUtils.convertTestStatusesToMap();
    	
    	//Format Statuses in Html
        String formattedStatus = MapToHtmlConverter.getHtmlFormattedStatus(map);
        String buildNumber = System.getenv("BUILD_NUMBER");
        String jobName = System.getenv("JOB_NAME");
        String gitUrl = System.getenv("GIT_URL");
        String buildUrl = System.getenv("BUILD_URL");
        //System.out.println("Build Number : " + System.getenv("BUILD_NUMBER"));
        
    	//Message Body
    	String subject = jobName + " - Automation Summary Report";
    	
    	//get the share-point link
    	ZipUtility util = new ZipUtility();
		String attachment = "testreports";
		File dir = new File(attachment);
		String zipDirName = attachment+".zip";
		util.zipDirectory(dir, zipDirName);

		String sharepointUrl = FileUpload.getLinkByFileId();
		
    	//String sharepointUrl = "gmail.com";
		String gitUrlFormatted = "<a title=\"GIT_URL\" href=\""+gitUrl+" \" target=\"_blank\" rel=\"noopener\">Git repository</a>";
		String jenkinsUrlFormatted = "<a title=\"BUILD_URL\" href=\""+buildUrl+" \" target=\"_blank\" rel=\"noopener\">Jenkins URL</a>";
		String mesage1 = "<p>Hi Team <br> The Jenkins <strong>build# "+buildNumber+"</strong> is executed succefully on "+jenkinsUrlFormatted+" for a new change commited on "+gitUrlFormatted+"</p>";
		String mesage2 = "<p>The Automation results generated from this job are uploaded to \"Google Drive\". Please refer to the below link for automation reports in zip fromat.</p>";
    	String url = "<p stype text-align: left;><a title=\"Automation Test Results\" href=\""+sharepointUrl+" \" target=\"_blank\" rel=\"noopener\">For Detailed Reports Click Here</a></p>";
        String headerHtml ="<p style=\"text-align: left;\"><strong>"+"Test Results Summary : "+"</strong><p>";
    	
        String bodyHtml = "";
        
        String signatureHtml = "<p><br>Thanks, <br>QA Team</p>";
        
        String strMessage = mesage1 + mesage2 + url + bodyHtml + headerHtml + formattedStatus + signatureHtml;
        
        final String username = "fullstackselenium@gmail.com";
        final String password = "Ramu@520";

        
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        //props.put("mail.smtp.host", "pop.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
        	
        	YmlUtils utils = new YmlUtils();
        	InternetAddress[] fromAddress = convertToInternetAddress(utils.getFromAddress());
        	InternetAddress[] toAddress = convertToInternetAddress(utils.getToAddress());
        	InternetAddress[] ccAddress = convertToInternetAddress(utils.getCcAddress());
        	
            Message message = new MimeMessage(session);
            message.setFrom(fromAddress[0]);
            message.setRecipients(Message.RecipientType.TO,toAddress);
            message.setRecipients(Message.RecipientType.CC,ccAddress);
            message.setSubject(subject);

            //Attach a document to the email
            BodyPart messageBodyPart= new MimeBodyPart();
            
            messageBodyPart.setContent(strMessage,"text/html");
            
            Multipart mutlipart = new MimeMultipart();
            mutlipart.addBodyPart(messageBodyPart);

            message.setContent(mutlipart);
            Transport.send(message);

            System.out.println("Email has been sent to QA distribution list");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

	private static InternetAddress[] convertToInternetAddress(String adress) { 
		
		if(adress.isEmpty())
			return null;
		
		//Declare InternetAddress variable
		InternetAddress[] internetAddressArray = null;
		
		//Split input String that ends with ";"
		String addressArray[]=adress.split(";");
	
		List<InternetAddress> listInternetAddress = new ArrayList<>();
		
		//Add valid address to array else skip it
		for (String address : addressArray) {
			
			if(address.endsWith("gmail.com")){
				try {
					listInternetAddress.add(new InternetAddress(address));
				} catch (AddressException e) {
					e.printStackTrace();
				}
			}
			internetAddressArray = listInternetAddress.stream().toArray(InternetAddress[]::new);
		}
		
		return internetAddressArray;
	}
}