package email;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import jsoup.HtmlUtils;
import yml.YmlUtils;


public class SendHtmlFormattedEmail_Bkup {

	public static void main(String[] args) throws Exception {

    	
    	//Get Statuses
    	HashMap<String,String> map = HtmlUtils.convertTestStatusesToMap();
    	
    	//Format Statuses in Html
        String formattedStatus = MapToHtmlConverter.getHtmlFormattedStatus(map);
    	
    	//Message Body
    	String subject = "Automation Summary Report";
    	String sharepointUrl = "gmail.com";
    	String mesage1 = "<p>Hi Team <br> Automation results are uploaded to \"Google Drive\". Please refer to the below link for automation reports in zip fromat.</p>";
    	String url = "<p stype text-align: left;><a title=\"Automation Test Results\" href=\""+sharepointUrl+" \" target=\"_blank\" rel=\"noopener\">For Detailed Reports Click Here</a></p>";
        String headerHtml ="<p style=\"text-align: left;\"><strong>"+"Test Results Summary : "+"</strong><p>";
    	
        String bodyHtml = "";
        
        String signatureHtml = "<p><br>Thanks, <br>Testing Team</p>";
        
        String strMessage = mesage1 + url + bodyHtml + headerHtml + formattedStatus + signatureHtml;
        
        final String username = "fullstackselenium@gmail.com";
        final String password = "Ramu@520";

        
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        //props.put("mail.smtp.host", "pop.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
        	
        	YmlUtils utils = new YmlUtils();
        	InternetAddress[] fromAddress = convertToInternetAddress(utils.getFromAddress());
        	InternetAddress[] toAddress = convertToInternetAddress(utils.getToAddress());
        	InternetAddress[] ccAddress = convertToInternetAddress(utils.getCcAddress());
        	
            Message message = new MimeMessage(session);
            message.setFrom(fromAddress[0]);
            message.setRecipients(Message.RecipientType.TO,toAddress);
            message.setRecipients(Message.RecipientType.CC,ccAddress);
            message.setSubject(subject);

            //Attach a document to the email
            BodyPart messageBodyPart= new MimeBodyPart();
            
            messageBodyPart.setContent(strMessage,"text/html");
            
            Multipart mutlipart = new MimeMultipart();
            mutlipart.addBodyPart(messageBodyPart);

            message.setContent(mutlipart);
            Transport.send(message);

            System.out.println("Email has been sent to QA distribution list");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

	private static InternetAddress[] convertToInternetAddress(String adress) { 
		
		if(adress.isEmpty())
			return null;
		
		//Declare InternetAddress variable
		InternetAddress[] internetAddressArray = null;
		
		//Split input String that ends with ";"
		String addressArray[]=adress.split(";");
	
		List<InternetAddress> listInternetAddress = new ArrayList<>();
		
		//Add valid address to array else skip it
		for (String address : addressArray) {
			
			if(address.endsWith("gmail.com")){
				try {
					listInternetAddress.add(new InternetAddress(address));
				} catch (AddressException e) {
					e.printStackTrace();
				}
			}
			internetAddressArray = listInternetAddress.stream().toArray(InternetAddress[]::new);
		}
		
		return internetAddressArray;
	}
}