package email;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class SendEmailWithAttachment {

    public static void main(String[] args) throws Exception {

        final String username = "fullstackselenium@gmail.com";
        final String password = "Ramu@520";

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

//        	InternetAddress[] fromAddressList = convertToInternetAddress(fromAdressList);
//        	InternetAddress[] toAddressList = convertToInternetAddress(fromAdressList);
//        	InternetAddress[] ccAddressList = convertToInternetAddress(fromAdressList);
        	
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ramesh.bodukani@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("ramesh520@gmail.com"));
//            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("ramesh.bodukani@cgi.com"));
            message.setSubject("Testing Subject");
            //message.setText("Dear Ramesh," + "\n\n This is a test email!!"+ "\n\n Regards,\n Ramesh Bodukani");
            
            String table =
            		"<style>\r\n" + 
            		"table, th, td {\r\n" + 
            		"  border:1px solid black;\r\n" + 
            		"}\r\n" + 
            		"</style>\r\n" + 
            		"<body>\r\n" + 
            		"\r\n" + 
            		"<h2>A basic HTML table</h2>\r\n" + 
            		"\r\n" + 
            		"<table style=\"width:100%\">\r\n" + 
            		"  <tr>\r\n" + 
            		"    <th>Company</th>\r\n" + 
            		"    <th>Contact</th>\r\n" + 
            		"    <th>Country</th>\r\n" + 
            		"  </tr>\r\n" + 
            		"  <tr>\r\n" + 
            		"    <td>Alfreds Futterkiste</td>\r\n" + 
            		"    <td>Maria Anders</td>\r\n" + 
            		"    <td>Germany</td>\r\n" + 
            		"  </tr>\r\n" + 
            		"  <tr>\r\n" + 
            		"    <td>Centro comercial Moctezuma</td>\r\n" + 
            		"    <td>Francisco Chang</td>\r\n" + 
            		"    <td>Mexico</td>\r\n" + 
            		"  </tr>\r\n" + 
            		"</table>\r\n" + 
            		"\r\n" + 
            		"<p>To undestand the example better, we have added borders to the table.</p>\r\n" + 
            		"\r\n" + 
            		"</body>\r\n" + 
            		           		"";
            //Attach a document to the email
            BodyPart messageBodyPart= new MimeBodyPart();
            String strMessage = "Dear Ramesh," + "\n\n This is a test email!!"+ table + "\n\n Regards,\n Ramesh Bodukani";
            
//            HashMap<String,String> map = new HashMap<>();
//            map.put("TC01", "PASS");
//            map.put("TC02", "FAIL");
//             
//            
//            String formattedStatus = MapToHtmlConvter.getHtmlFormattedStatus(map);
            
            messageBodyPart.setContent(strMessage,"text/html");
            
            //Add attachment
            FileDataSource source = new FileDataSource("C:\\Users\\bodukanir\\Desktop\\Ramesh Bodukani_Selenium Automation_5YearsExp.docx");
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName("C:\\Users\\bodukanir\\Desktop\\Ramesh Bodukani_Selenium Automation_5YearsExp.docx");
            
            Multipart mutlipart = new MimeMultipart();
            mutlipart.addBodyPart(messageBodyPart);

            message.setContent(mutlipart);
            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

	private static InternetAddress[] convertToInternetAddress(String adressList) { 
		
		if(adressList.isEmpty())
			return null;
		
		//Declare InternetAddress variable
		InternetAddress[] internetAddressArray = null;
		
		//Split input String that ends with ";"
		String addressArray[]=adressList.split(";");
	
		List<InternetAddress> listInternetAddress = new ArrayList<>();
		
		//Add valid address to array else skip it
		for (String address : addressArray) {
			
			if(address.endsWith("cgi.com")){
				try {
					listInternetAddress.add(new InternetAddress(address));
				} catch (AddressException e) {
					e.printStackTrace();
				}
			}
			internetAddressArray = listInternetAddress.stream().toArray(InternetAddress[]::new);
		}
		
		return internetAddressArray;
	}
}