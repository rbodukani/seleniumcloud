package email;

import java.util.HashMap;
import java.util.Map;

public class MapToHtmlConverter {
	
	public static String getHtmlFormattedStatus(HashMap<String,String> reportStatuses) throws Exception{

	String tableStartHtml ="<table style=\"height:15px; width:50%; border: 1px solid black\">";
	String tableEndHtml="</table>";
	String theadStartAndEndHtml = "<thead>"
			+ "<tr style=\"height:15px; width: 400px border: 1px solid orange\">"
			+ "<td style=\"width:35px; height:15px; text-align: center; padding: 10px;background-color: LightGray; \"><strong><span style=\"color: Black \">Sl.No</span></strong></td>"
			+ "<td style=\"width:300px; height:15px; text-align: left;padding: 10px;background-color: LightGray; \"><strong><span style=\"color: Black \">Test Script</span></strong></td>"
			+ "<td style=\"width:100px; height:15px; text-align: center; padding: 10px;background-color: LightGray;\"><strong><span style=\"color: Black \">Execution Status</span></strong></td>"
			+ "</thead>";
	
	String tbodyStartHtml ="<tbody>";
	String tbodyEndHtml ="</tbody>";
	
	String tableContent = null;
	
	int row=1;
	String rowColor = null;
	String result = null;
	
	for (Map.Entry m: reportStatuses.entrySet()) {
		result = (String) m.getValue();

		if(result.equalsIgnoreCase("PASS")) {
			rowColor = "background-color: #bff2bf";
		} else if(result.equalsIgnoreCase("FAIL")) {
			rowColor = "background-color: #ffcccc";
		}
		
		if(tableContent == null) {
			
			tableContent = "<tr style=\"height: 15px;width: 400px;"+rowColor+" \">" 
					+"<td style = \"width: 35px; height: 15px; text-align: center; padding: 10px;\">" + row + "</td>"
					+"<td style = \"width: 300px; height: 15px; text-align: left; padding: 10px;\">" + m.getKey() + "</td>"
					+"<td style = \"width: 100px; height: 15px; text-align: center; padding: 10px;\">" + m.getValue()+ "</td>"
					+"</tr>";
		row++;
		} else {
			tableContent = tableContent + "<tr style=\"height: 30px; width: 600px;"+rowColor+" \">" 
					+"<td style = \"width: 35px; height: 15px; text-align: center; padding: 10px;\">" + row + "</td>"
					+"<td style = \"width: 300px; height: 15px; text-align: left; padding: 10px;\">" + m.getKey() + "</td>"
					+"<td style = \"width: 100px; height: 15px; text-align: center; padding: 10px;\">" + m.getValue()+ "</td>"
					+"</tr>";
			row++;
		}
	}
	
	String htmlDocument = tableStartHtml+theadStartAndEndHtml+tbodyStartHtml+tableContent+tbodyEndHtml+tableEndHtml;
	
	return htmlDocument;
}
}