package jsoup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HtmlUtils {

	public static HashMap<String, String> convertTestStatusesToMap() throws Exception {

		File reportFiles[] = new File("testreports").listFiles();
		HashMap<String, String> map = new HashMap<String, String>();

		for (File reportFile : reportFiles) {
			System.out.println("Files " + reportFile.getAbsolutePath());

			if (reportFile.isDirectory())
				continue;

			String xmlFile = reportFile.getAbsolutePath();
			String FileName = reportFile.getName();
			StringBuilder contentBuilder = new StringBuilder();
			try {
				BufferedReader in = new BufferedReader(new FileReader(xmlFile));
				String str;

				while ((str = in.readLine()) != null) {
					contentBuilder.append(str);
				}
				in.close();

			} catch (Exception e) {
			}
			String content = contentBuilder.toString();
			Document document = Jsoup.parse(content);
			Elements strtestCases = document.select("div[class=test-detail]");
			for (Element testcase : strtestCases) {

				String testCaseName = testcase.select("p[class=name]").text();
				String testCaseStatus = testcase.select("p[class=text-sm] > span[class]").text();
				map.put(testCaseName, testCaseStatus);

			}
		}
		
		if (map.isEmpty()) {
			map.put("No Data Available in the testreports folder", "");
		}
		
		return map;
	}
}
